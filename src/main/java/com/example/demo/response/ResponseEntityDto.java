package com.example.demo.response;

import org.springframework.http.HttpStatus;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class ResponseEntityDto {
	
	private String message;
	
	private HttpStatus status;
	
	private Object data;
	
	public void setVariables(String message, HttpStatus status, Object data) {
		this.message = message;
		this.status = status;
		this.data = data;
	}
}
