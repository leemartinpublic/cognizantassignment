package com.example.demo.student.dao;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@Entity
@Table(name="Student")
public class Student {
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "Student_id")
	private Long studentId;
	
	@Column(name = "Student_email")
	private String studentEmail;
	
	@Column(name = "Is_suspended")
	private Integer isSuspended;
	
	@Column(name = "Role_id")
	private Integer roleId;
}
