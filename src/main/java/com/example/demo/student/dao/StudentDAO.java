package com.example.demo.student.dao;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

public interface StudentDAO extends JpaRepository<Student, Long>{
	
	@Query(value="SELECT * FROM Student WHERE Student_email=:studentEmail", nativeQuery=true)
	public Student findStudentByEmail(@Param("studentEmail") String studentEmail);
	
	@Query(value="SELECT * FROM Student s WHERE Is_suspended=0 AND s.Student_email IN :listOfStudentEmail OR s.Student_email IN (SELECT r.Student_email FROM Relationship r WHERE Teacher_email=:teacherEmail)", nativeQuery=true)
	public List<Student> findStudentsByEmailListAndByRegisteredTeacher(@Param("listOfStudentEmail") List<String> listOfStudentEmail, @Param("teacherEmail") String teacherEmail);
}
