package com.example.demo.teacher.dto;

import java.util.List;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class RegisterStudentsRequest {
	
	private String teacher;
	
	private List<String> students;
}
