package com.example.demo.teacher.dto;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class RetrieveForNotificationsRequest {
	
	private String teacher;
	
	private String notification;
}
