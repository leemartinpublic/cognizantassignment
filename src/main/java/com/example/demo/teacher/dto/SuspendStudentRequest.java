package com.example.demo.teacher.dto;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class SuspendStudentRequest {
	private String student;
}
