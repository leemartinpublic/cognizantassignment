package com.example.demo.teacher.service;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;

import com.example.demo.constants.ResponseFailureMessage;
import com.example.demo.constants.ResponseSuccessMessage;
import com.example.demo.constants.RoleConstants;
import com.example.demo.relationship.dao.Relationship;
import com.example.demo.relationship.dao.RelationshipDAO;
import com.example.demo.response.ResponseEntityDto;
import com.example.demo.student.dao.Student;
import com.example.demo.student.dao.StudentDAO;
import com.example.demo.teacher.dao.TeacherDAO;
import com.example.demo.teacher.dto.RegisterStudentsRequest;
import com.example.demo.teacher.dto.RetrieveForNotificationsRequest;
import com.example.demo.teacher.dto.StudentListDTO;

@Service
public class TeacherService {
	
	@Autowired
	private RelationshipDAO relationshipDAO;
	
	@Autowired
	private StudentDAO studentDAO;
	
	@Autowired
	private TeacherDAO teacherDAO;
	
	public ResponseEntityDto registerStudents(RegisterStudentsRequest registerStudentsRequest){
		String teacherEmail = registerStudentsRequest.getTeacher();
		List<String> students = registerStudentsRequest.getStudents();
		String responseMessage = "Sucess";
		ResponseEntityDto responseEntityDto = new ResponseEntityDto();
		
		if(teacherDAO.getListOfTeachers(Arrays.asList(teacherEmail)) == null) {
			responseMessage = ResponseFailureMessage.TEACHER_DOES_NOT_EXIST.message;
			responseEntityDto.setVariables(responseMessage, HttpStatus.BAD_REQUEST, null);
			return responseEntityDto;
		}
		
		if (students == null){
			responseMessage = ResponseFailureMessage.NO_STUDENT_EMAIL_IN_REQUEST.message;
			responseEntityDto.setVariables(responseMessage, HttpStatus.BAD_REQUEST, null);
			return responseEntityDto;
		}
		
		for (int i = 0; i < students.size(); i++) {
			String studentEmail = students.get(i);
			
			Student student = studentDAO.findStudentByEmail(studentEmail);
			Relationship relationship = relationshipDAO.findRelationshipByTeacherEmailAndStudentEmail(teacherEmail, studentEmail);
			
			if (student == null) {
				student = new Student();
				student.setIsSuspended(0);
				student.setRoleId(RoleConstants.ROLE_STUDENT.roleId);
				student.setStudentEmail(studentEmail);
				studentDAO.saveAndFlush(student);
			}
			
			if (relationship == null) {
				relationship = new Relationship();
				relationship.setStudentEmail(studentEmail);
				relationship.setTeacherEmail(teacherEmail);
				
				relationshipDAO.saveAndFlush(relationship);
			}
			
		}
		
		responseMessage = ResponseSuccessMessage.STUDENT_SUCCESSFULLY_REGISTERED.message;
		responseEntityDto.setVariables(responseMessage, HttpStatus.NO_CONTENT, null);
		return responseEntityDto;
		
	}
	
	public ResponseEntityDto getListOfCommonStudents(List<String> listOfTeacherEmail){
		String responseMessage = "Sucess";
		ResponseEntityDto responseEntityDto = new ResponseEntityDto();
		
		if (listOfTeacherEmail == null){
			responseMessage = ResponseFailureMessage.NO_TEACHER_EMAIL_IN_REQUEST.message;
			responseEntityDto.setVariables(responseMessage, HttpStatus.BAD_REQUEST, null);
			return responseEntityDto;
		}
		
		for (int i = 0; i < listOfTeacherEmail.size(); i++) {
			if (listOfTeacherEmail.get(i) == null) {
				responseMessage = ResponseFailureMessage.NO_TEACHER_EMAIL_IN_REQUEST.message;
				responseEntityDto.setVariables(responseMessage, HttpStatus.BAD_REQUEST, null);
				return responseEntityDto;
			}
		}
		
		List<String> results = teacherDAO.getListOfCommonStudentByListOfTeachers(listOfTeacherEmail, listOfTeacherEmail.size());
		StudentListDTO sListDto = new StudentListDTO();
		sListDto.setStudents(results);
		
		responseMessage = ResponseSuccessMessage.SUCCESSFULLY_FETCHED_COMMON_LIST_OF_STUDENTS.message;
		responseEntityDto.setVariables(responseMessage, HttpStatus.OK, sListDto);
		
		return responseEntityDto;
	}
	
	@Transactional
	public ResponseEntityDto suspendStudent(String studentEmail){
		String responseMessage = "Sucess";
		ResponseEntityDto responseEntityDto = new ResponseEntityDto();
		
		//checks if student exist
		Student s = studentDAO.findStudentByEmail(studentEmail);
		
		if (s == null){
			responseMessage = ResponseFailureMessage.NO_STUDENT_EMAIL_IN_REQUEST.message;
			responseEntityDto.setVariables(responseMessage, HttpStatus.BAD_REQUEST, null);
			return responseEntityDto;
		}
		
		teacherDAO.suspendStudent(s.getStudentEmail());
		
		responseMessage = ResponseSuccessMessage.SUCCESSFULLY_SUSPENDED_STUDENT.message;
		responseEntityDto.setVariables(responseMessage, HttpStatus.NO_CONTENT, null);
		
		return responseEntityDto;
	}
	
	public ResponseEntityDto retrieveForNotifications(RetrieveForNotificationsRequest retrieveForNotificationsRequest){
		String teacherEmail = retrieveForNotificationsRequest.getTeacher();
		String notification = retrieveForNotificationsRequest.getNotification();
		String responseMessage = "Sucess";
		ResponseEntityDto responseEntityDto = new ResponseEntityDto();
		
		if (teacherEmail == null) {
			responseMessage = ResponseFailureMessage.NO_TEACHER_EMAIL_IN_REQUEST.message;
			responseEntityDto.setVariables(responseMessage, HttpStatus.BAD_REQUEST, null);
			return responseEntityDto;
		}
		
		if (notification == null) {
			responseMessage = ResponseFailureMessage.NO_NOTIFICATIONS_IN_REQUEST.message;
			responseEntityDto.setVariables(responseMessage, HttpStatus.BAD_REQUEST, null);
			return responseEntityDto;
		}
		
		List<String> listOfStudentEmail = new ArrayList<>();
		
		//use Regex to get emails that are mentioned
		Matcher matcher = Pattern.compile("[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,6}").matcher(notification);
		while (matcher.find()) {
			listOfStudentEmail.add(matcher.group());
       }
		
		List<Student> students = studentDAO.findStudentsByEmailListAndByRegisteredTeacher(listOfStudentEmail, teacherEmail);
		
		responseMessage = ResponseSuccessMessage.SUCCESSFULLY_FETCHED_LIST_OF_STUDENTS_TO_BE_NOTIFIED.message;
		responseEntityDto.setVariables(responseMessage, HttpStatus.OK, students);
		
		return responseEntityDto;
		
	}
	
	
}
