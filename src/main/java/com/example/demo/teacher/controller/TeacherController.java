package com.example.demo.teacher.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import com.example.demo.response.ResponseEntityDto;
import com.example.demo.response.ResponseHandler;
import com.example.demo.teacher.dto.RegisterStudentsRequest;
import com.example.demo.teacher.dto.RetrieveForNotificationsRequest;
import com.example.demo.teacher.dto.SuspendStudentRequest;
import com.example.demo.teacher.service.TeacherService;

@Controller
@RequestMapping("/api")
public class TeacherController {
	
	@Autowired
	private TeacherService teacherService;
	
	@GetMapping
	public String index() {
		return "Greetings from Spring Boot!";
	}
	
	@PostMapping("/register")
	public ResponseEntity<Object> registerStudents(@RequestBody RegisterStudentsRequest registerStudentsRequest) {
		
		ResponseEntityDto r = teacherService.registerStudents(registerStudentsRequest);
		return ResponseHandler.buildResponse(r.getMessage(), r.getStatus(), r.getData());
	}
	
	//get request from url
	@GetMapping("/commonstudents")
	public ResponseEntity<Object> getListOfCommonStudents(@RequestParam List<String> teacher) {
		ResponseEntityDto r = teacherService.getListOfCommonStudents(teacher);
		return ResponseHandler.buildResponse(r.getMessage(), r.getStatus(), r.getData());
	}
	
	@PostMapping("/suspend")
	public ResponseEntity<Object> suspendStudent(@RequestBody SuspendStudentRequest suspendStudentRequest){
		ResponseEntityDto r = teacherService.suspendStudent(suspendStudentRequest.getStudent());
		return ResponseHandler.buildResponse(r.getMessage(), r.getStatus(), r.getData());
	}
	
	@PostMapping("/retrievefornotifications")
	public ResponseEntity<Object> retrieveForNotifications(@RequestBody RetrieveForNotificationsRequest retrieveForNotificationsRequest){
		ResponseEntityDto r = teacherService.retrieveForNotifications(retrieveForNotificationsRequest);
		return ResponseHandler.buildResponse(r.getMessage(), r.getStatus(), r.getData());
	}
	
}
