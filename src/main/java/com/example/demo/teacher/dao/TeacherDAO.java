package com.example.demo.teacher.dao;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import com.example.demo.student.dao.Student;

public interface TeacherDAO extends JpaRepository<Teacher, Long>{
	
	@Query(value="SELECT t.Teacher_email FROM Teacher t where Teacher_email IN :listOfTeachers", nativeQuery=true)
	public List<String> getListOfTeachers(@Param("listOfTeachers") List<String> listOfTeachers);
	
	@Query(value="SELECT s.Student_email FROM Student s WHERE s.Student_email IN (SELECT r.Student_email FROM Relationship r WHERE r.Teacher_email IN :listOfTeachers GROUP BY Student_email HAVING COUNT(*)=:listOfTeachersSize)", nativeQuery=true)
	public List<String> getListOfCommonStudentByListOfTeachers(@Param("listOfTeachers") List<String> listOfTeachers, @Param("listOfTeachersSize") Integer listOfTeachersSize);
	
	@Modifying
	@Query(value="UPDATE Student SET Is_suspended = 1 WHERE Student_email=:student", nativeQuery=true)
	public void suspendStudent(@Param("student") String student);
	
	@Query(value="SELECT * FROM Student WHERE Is_suspended = 0 IN (SELECT * FROM Relationship WHERE Teacher_email=:teacherEmail AND Student_email IN :students) OR Student_email IN :students", nativeQuery=true)
	public List<Student> getListOfNotifiedStudent(@Param("teacherEmail") String teacherEmail, @Param("students") List<String> students);

}
