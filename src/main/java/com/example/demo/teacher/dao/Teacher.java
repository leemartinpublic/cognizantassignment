package com.example.demo.teacher.dao;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@Entity
public class Teacher {
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "Teacher_id")
	private Long teacherId;
	
	@Column(name = "Teacher_email")
	private String teacherEmail;
	
	@Column(name = "Role_id")
	private Integer roleId;
}
