package com.example.demo.relationship.dao;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@Entity
@Table(name="Relationship")
public class Relationship {
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "Relationship_id")
	private String relationshipId;
	
	@Column(name = "Teacher_email")
	private String teacherEmail;
	
	@Column(name = "Student_email")
	private String studentEmail;
}
