package com.example.demo.relationship.dao;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

public interface RelationshipDAO extends JpaRepository<Relationship, Integer>{
	
	@Query(value="SELECT * FROM Relationship WHERE Teacher_email=:teacherEmail AND Student_email=:studentEmail", nativeQuery=true)
	public Relationship findRelationshipByTeacherEmailAndStudentEmail(@Param("teacherEmail") String teacherEmail, @Param("studentEmail") String studentEmail);
	
	@Query(value="SELECT * FROM Relationship WHERE Teacher_email=:teacherEmail AND Student_email IN :listOfStudent", nativeQuery=true)
	public Relationship findRelationshipByTeacherEmail(@Param("teacherEmail") String teacherEmail, @Param("listOfStudent") List<String> listOfStudent);
}
