package com.example.demo.constants;

public enum ResponseSuccessMessage {
	STUDENT_SUCCESSFULLY_REGISTERED("Students are successfully registered"),
	SUCCESSFULLY_FETCHED_COMMON_LIST_OF_STUDENTS("Common list of students are successfully retrieved"),
	SUCCESSFULLY_SUSPENDED_STUDENT("Student is successfully suspended"),
	SUCCESSFULLY_FETCHED_LIST_OF_STUDENTS_TO_BE_NOTIFIED("Successfully retrieved list of students to be notified");
	
	public final String message;

	ResponseSuccessMessage(String message) {
		this.message = message;
	}
	
}
