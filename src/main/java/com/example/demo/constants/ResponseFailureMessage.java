package com.example.demo.constants;

public enum ResponseFailureMessage {
	TEACHER_DOES_NOT_EXIST("Teacher does not exist"),
	NO_STUDENT_EMAIL_IN_REQUEST("No student email specified in request"),
	NO_TEACHER_EMAIL_IN_REQUEST("No teacher email specified in request"),
	NO_NOTIFICATIONS_IN_REQUEST("No notifications specified in request");
	
	public final String message;

	ResponseFailureMessage(String message) {
		this.message = message;
	}
	
}
