package com.example.demo.constants;

public enum RoleConstants {
	ROLE_TEACHER(1),
	ROLE_STUDENT(2);
	
	public final Integer roleId;

	RoleConstants(Integer roleId) {
		this.roleId = roleId;
	}
	
}
