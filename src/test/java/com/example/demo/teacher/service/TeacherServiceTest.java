package com.example.demo.teacher.service;

import static org.junit.jupiter.api.Assertions.assertEquals;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.example.demo.constants.ResponseFailureMessage;
import com.example.demo.constants.ResponseSuccessMessage;
import com.example.demo.relationship.dao.RelationshipDAO;
import com.example.demo.response.ResponseEntityDto;
import com.example.demo.student.dao.Student;
import com.example.demo.student.dao.StudentDAO;
import com.example.demo.teacher.dao.TeacherDAO;
import com.example.demo.teacher.dto.RegisterStudentsRequest;
import com.example.demo.teacher.dto.RetrieveForNotificationsRequest;

@SpringBootTest
@RunWith(SpringJUnit4ClassRunner.class)
public class TeacherServiceTest {
	
	@MockBean 
	private TeacherDAO teacherDAO;
	
	@MockBean 
	private RelationshipDAO relationshipDAO;
	
	@MockBean 
	private StudentDAO studentDAO;
	
	@Autowired
	private TeacherService teacherService;
	
	@Test
	void testRegisterStudent(){
		RegisterStudentsRequest registerStudentsRequest = new RegisterStudentsRequest();
		//throws exception if teacher does not exist
		Mockito.when(teacherDAO.getListOfTeachers(Arrays.asList(registerStudentsRequest.getTeacher()))).thenReturn(null);
		ResponseEntityDto response1 = teacherService.registerStudents(registerStudentsRequest);
		assertEquals(response1.getMessage(), ResponseFailureMessage.TEACHER_DOES_NOT_EXIST.message);
		
		//throws exception if teacher exist but student email from request is null
		Mockito.when(teacherDAO.getListOfTeachers(Arrays.asList(registerStudentsRequest.getTeacher()))).thenReturn(Arrays.asList("teacherken@gmail.com"));
		ResponseEntityDto response2 = teacherService.registerStudents(registerStudentsRequest);
		assertEquals(response2.getMessage(), ResponseFailureMessage.NO_STUDENT_EMAIL_IN_REQUEST.message);
		
		//success if teacher exist and student email from request is not null
		registerStudentsRequest.setStudents(Arrays.asList("studentjon@gmail.com"));
		Mockito.when(teacherDAO.getListOfTeachers(Arrays.asList(registerStudentsRequest.getTeacher()))).thenReturn(Arrays.asList("teacherken@gmail.com"));
		ResponseEntityDto response3 = teacherService.registerStudents(registerStudentsRequest);
		assertEquals(response3.getMessage(), ResponseSuccessMessage.STUDENT_SUCCESSFULLY_REGISTERED.message);
	}
	
	@Test
	void testGetListOfCommonStudents(){
		List<String> teacher = null;
		//throws exception if teacher email is not specified in request 
		ResponseEntityDto response1 = teacherService.getListOfCommonStudents(teacher);
		assertEquals(response1.getMessage(), ResponseFailureMessage.NO_TEACHER_EMAIL_IN_REQUEST.message);
		
		//throws exception if one of the teacher email is null in the request
		teacher = new ArrayList<>();
		teacher.add("teacherken@gmail.com");
		teacher.add(null);
		ResponseEntityDto response2 = teacherService.getListOfCommonStudents(teacher);
		assertEquals(response2.getMessage(), ResponseFailureMessage.NO_TEACHER_EMAIL_IN_REQUEST.message);
		
		//success if list of teacher email in the request is not null and does not contain any null elements
		teacher.remove(null);
		teacher.add("teacherjoe@gmail.com");
		ResponseEntityDto response3 = teacherService.getListOfCommonStudents(teacher);
		assertEquals(response3.getMessage(), ResponseSuccessMessage.SUCCESSFULLY_FETCHED_COMMON_LIST_OF_STUDENTS.message);
	}
	
	@Test
	void testSuspendStudent(){
		//throws exception if student email from request is null
		String studentRequest = null;
		Mockito.when(studentDAO.findStudentByEmail(studentRequest)).thenReturn(null);
		ResponseEntityDto response2 = teacherService.suspendStudent(studentRequest);
		assertEquals(response2.getMessage(), ResponseFailureMessage.NO_STUDENT_EMAIL_IN_REQUEST.message);
		
		//success if teacher exist and student email from request is not null
		studentRequest="studentjon@gmail.com";
		Student student = new Student();
		student.setStudentEmail(studentRequest);
		student.setIsSuspended(0);
		student.setStudentId(1L);
		student.setRoleId(2);
		Mockito.when(studentDAO.findStudentByEmail(studentRequest)).thenReturn(student);
		ResponseEntityDto response3 = teacherService.suspendStudent(studentRequest);
		assertEquals(response3.getMessage(), ResponseSuccessMessage.SUCCESSFULLY_SUSPENDED_STUDENT.message);
	}
	
	@Test
	void testRetrieveForNotifications() {
		RetrieveForNotificationsRequest retrieveForNotificationsRequest = new RetrieveForNotificationsRequest();
		retrieveForNotificationsRequest.setNotification(null);
		retrieveForNotificationsRequest.setTeacher(null);
		//throws exception if teacher email is not specified in request 
		ResponseEntityDto response1 = teacherService.retrieveForNotifications(retrieveForNotificationsRequest);
		assertEquals(response1.getMessage(), ResponseFailureMessage.NO_TEACHER_EMAIL_IN_REQUEST.message);
		
		retrieveForNotificationsRequest.setTeacher("teacherken@gmail.com");
		//throws exception if notification is not specified in request 
		ResponseEntityDto response2 = teacherService.retrieveForNotifications(retrieveForNotificationsRequest);
		assertEquals(response2.getMessage(), ResponseFailureMessage.NO_NOTIFICATIONS_IN_REQUEST.message);
		
		//success if teacher email and notifications is not null in the request is not null
		retrieveForNotificationsRequest.setNotification("Hi everyone!");
		Student student = new Student();
		student.setStudentEmail("studentjon@gmail.com");
		student.setIsSuspended(0);
		student.setStudentId(1L);
		student.setRoleId(2);
		Mockito.when(studentDAO.findStudentsByEmailListAndByRegisteredTeacher(Arrays.asList(student.getStudentEmail()), retrieveForNotificationsRequest.getTeacher())).thenReturn(Arrays.asList(student));
		ResponseEntityDto response3 = teacherService.retrieveForNotifications(retrieveForNotificationsRequest);
		assertEquals(response3.getMessage(), ResponseSuccessMessage.SUCCESSFULLY_FETCHED_LIST_OF_STUDENTS_TO_BE_NOTIFIED.message);
	}
}
