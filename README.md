
## API Reference

#### Get all items

```http
  POST /api/register
```

| Parameter | Type     | Description                |
| :-------- | :------- | :------------------------- |
| `teacher` | `string` | **Required** |
| `students` | `List<string>` | **Required** |

#### Get item

```http
  GET /api/commonstudents?teacher=teacherken%40gmail.com
```

| Parameter | Type     | Description                       |
| :-------- | :------- | :-------------------------------- |
| `teacher`      | `string` | **Required**|

```http
   POST /api/suspend
```

| Parameter | Type     | Description                       |
| :-------- | :------- | :-------------------------------- |
| `student`      | `string` | **Required**|

```http
  POST /api/retrievefornotifications
```

| Parameter | Type     | Description                       |
| :-------- | :------- | :-------------------------------- |
| `teacher`      | `string` | **Required**|
| `notification`      | `string` | **Required**|


